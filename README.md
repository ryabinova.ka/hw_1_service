# hw_1_service

Сервис по классификации картинок из компетишена [Planet: Understanding the Amazon from Space](https://www.kaggle.com/c/planet-understanding-the-amazon-from-space/overview)

## Описание API, примеры запросов

### Проверка того, что сервис запущен
```
GET 0.0.0.0:5000/health_check
```

### Классы

```
GET 0.0.0.0:5000/classes
```
На выходе будут все доступные классы

### Предсказать классы по картинке

```
POST 0.0.0.0:5000/predict <RGB картинка в байтах>
```

### Предсказать скоры классов по картинке

```
POST 0.0.0.0:5000/predict_proba <RGB картинка в байтах>
```

## Как развернуть сервис локально

### Локально без докера

1. Подготовка окружения

```bash
cd app
python3 -m venv /path/to/new/virtual/environment
source /path/to/new/virtual/environment/bin/activate
```

2. Установка зависимостей

Находясь в активированном окружении, выполните:

```bash
pip install -r requirements.txt
```

3. Загрузка весов сети

```bash
dvc pull
```

4. Запуск приложения

```bash
./run.sh
```
Сервис будет доступен по URL 0.0.0.0:5000

### Локально с докером

```bash
cd /путь/до/директории/проекта
docker build -t rainforest_service:1.0 .
docker run -p 1234:5000 rainforest_service:1.0
```

Сервис будет доступен по URL 0.0.0.0:1234

## Как запустить тесты

```bash
cd app/
PYTHONPATH=. pytest
```