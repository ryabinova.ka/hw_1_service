import os

import cv2
import numpy as np
import pytest
from omegaconf import OmegaConf
from PIL import Image

from run_app import create_app
from service.core.models.rainforest_classifier import RainforestClassifier

images = ['train_222.jpg', 'train_465.jpg']
folder_images = 'tests/test_data'


def image_in_byte(image: str) -> bytes:
    """Конвертим картинку в байты для тестирования.

    Args:
        image (str): путь до картинки

    Returns:
        картинка в байтах
    """
    fname = os.path.join(folder_images, image)
    image = cv2.imread(fname)[..., ::-1]
    Image.fromarray(image)
    flag, image_encoded = cv2.imencode('.png', image)
    return image_encoded.tobytes()


@pytest.fixture(scope='session')
def config():
    """Фикстура для конфига.

    Returns:
        конфиг в виде словаря
    """
    return OmegaConf.load('config/config.yml')


@pytest.fixture(scope='session')
def model_classifier(config):  # noqa: WPS442
    """Фикстура для модели.

    Args:
        config (dict): конфиг в виде словаря

    Returns:
        модель для тестирования
    """
    model = RainforestClassifier(
        config['rainforest_analytics']['models']['rainforest_classifier'],
    )
    input_data = np.ones((*model.size, 3), dtype=np.uint8)
    model.predict(input_data)
    return model


@pytest.fixture
def test_images_in_byte():
    """Возвращаем список картинок в байтах.

    Returns:
        картинки в байтах
    """
    return list(map(image_in_byte, images))


@pytest.fixture(scope='session')
def client():
    """Фикстура для тестирования приложения.

    Yields:
        текстовый клиент
    """
    app = create_app()

    with app.test_client() as client:  # noqa: WPS442
        yield client
