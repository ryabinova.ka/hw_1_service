import cv2
import numpy as np


def test_model_classifier_on_predict(
    model_classifier,
    test_images_in_byte,
):
    image = cv2.imdecode(
        np.frombuffer(test_images_in_byte[0], np.uint8),
        cv2.IMREAD_COLOR,
    )
    predict = model_classifier.predict(image)
    assert predict[0] is not None
