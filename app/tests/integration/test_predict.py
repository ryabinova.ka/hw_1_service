import http


def test_image_predict(client, test_images_in_byte):
    """Тестируем то, что модель может предсказать что-то.

    Args:
        client (test client): тестовый клиент из фикстуры
        test_images_in_byte (bytes): картинка из фикстуры
    """
    rv = client.post('/predict', data=test_images_in_byte[0])
    assert rv.status_code == http.HTTPStatus.OK


def test_health_check(client):
    """Тестируем health_check.

    Args:
        client (test client): тестовый клиент из фикстуры
    """
    rv = client.get('/health_check')
    assert rv.status_code == http.HTTPStatus.OK


def test_classes(client):
    """Тестируем classes.

    Args:
        client (test client): тестовый клиент из фикстуры
    """
    rv = client.get('/classes')
    assert rv.status_code == http.HTTPStatus.OK
