"""Entrypoint."""

import os

import sentry_sdk
from flask import Flask
from omegaconf import OmegaConf
from sentry_sdk.integrations.flask import FlaskIntegration

from service.containers import Container
from service.routes import health_check
from service.routes import predict as model_routes


def create_app() -> Flask:
    """Входная точка сервиса.

    Returns:
        Flask app
    """
    app = Flask(__name__)
    # работа с конфигами
    if not OmegaConf.has_resolver('weights_path'):
        OmegaConf.register_new_resolver('weights_path', _weights_path)
    cfg = OmegaConf.load(_abs_path('config/config.yml'))
    # инициализируем контейнер
    container = Container()
    container.config.from_dict(cfg)
    container.wire(modules=[model_routes])
    init_sentry(cfg['sentry']['dsn'])
    set_routes(app)
    return app


def init_sentry(dsn: str) -> None:
    """Инициализуем sentry.

    Args:
        dsn (str): креды
    """
    sentry_sdk.init(
        dsn=dsn,
        integrations=[FlaskIntegration()],

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production.
        traces_sample_rate=1.0,
    )


def set_routes(app: Flask):
    """Объявляем все роуты.

    Args:
        app (Flask): Flask
    """
    app.add_url_rule(
        '/health_check',
        'health_check',
        health_check.health_check,
        methods=['GET'],
    )
    app.add_url_rule(
        '/predict',
        'predict',
        model_routes.predict,
        methods=['POST'],
    )
    app.add_url_rule(
        '/predict_proba',
        'predict_proba',
        model_routes.predict_proba,
        methods=['POST'],
    )
    app.add_url_rule(
        '/classes',
        'classes',
        model_routes.classes_list,
        methods=['GET'],
    )


def _abs_path(local_path: str) -> str:

    proj_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(proj_dir, local_path)


def _weights_path(local_path: str) -> str:
    return os.path.join(_abs_path('weights'), local_path)


app = create_app()
