from functools import cached_property
from typing import Dict

from service.core.models.rainforest_classifier import RainforestClassifier


class ModelZoo:
    """Класс хранящий модель."""
    def __init__(self, config: Dict):
        """Инициализируем класс с моделями.

        Args:
            config (Dict): конфиг для развертки моделей
        """
        self.config = config

    @cached_property
    def rainforest_classifier(self):
        """Классификатор для rainforest.

        Returns:
            Объект классфикатора
        """
        return RainforestClassifier(self.config['rainforest_classifier'])
