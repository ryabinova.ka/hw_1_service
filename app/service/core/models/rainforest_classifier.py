from typing import Dict, List, Tuple

import numpy as np
import torch
import torch.nn as nn  # noqa: WPS301

from service.core.models.model_utils import preprocess_image


class RainforestClassifier:  # noqa: WPS214
    """Класс для предсказания влияния человека на тропический лес."""
    def __init__(self, config: Dict):
        """Инициализация класса.

        Args:
            config (Dict): словарь с настройками, например:

        config = {
            'model_path': 'weights/classification_v1.pt',
            'device': "cuda:0",
        }  # noqa: RST203
        """
        self._model_path = config['model_path']
        self._device = config['device']

        self._model: nn.Module = torch.jit.load(  # noqa: RST203
            self._model_path,
            map_location='cpu',
        )
        self._classes: np.ndarray = np.array(self._model.classes)
        self._size: Tuple[int, int] = self._model.size
        self._thresholds: np.ndarray = np.array(self._model.thresholds)

    @property
    def classes(self) -> List[str]:
        """Возвращает список классов.

        Returns:
            Список классов
        """
        return list(self._classes)

    @property
    def size(self) -> Tuple:
        """Возвращает размер модели.

        Returns:
            размер
        """
        return self._size

    def predict(self, image: np.ndarray) -> List[str]:
        """Предсказание классов из компетишена.

        Args:
            image (np.ndarray): RGB изображение

        Returns:
            список классов
        """
        return self._postprocess_predict(self._predict(image))

    def predict_proba(self, image: np.ndarray) -> Dict[str, float]:
        """Предсказание вероятностей принадлежности к классам.

        Args:
            image (np.ndarray): RGB изображение

        Returns:
            словарь вида класс: вероятность.
        """
        return self._postprocess_predict_proba(self._predict(image))

    def _predict(self, image: np.ndarray) -> np.ndarray:
        """Предсказание вероятностей.

        Args:
            image (np.ndarray): RGB изображение

        Returns:
            вероятности
        """
        batch = preprocess_image(image, self._size)

        # прогон батча через сеть
        with torch.no_grad():
            model_predict = self._model(batch.to(self._device)).detach().cpu()[0]  # noqa: E501

        return model_predict.numpy()

    def _postprocess_predict(self, predict: np.ndarray) -> List[str]:
        """Постобработка.

        Args:
            predict (np.ndarray): вероятности

        Returns:
            список классов
        """
        return self._classes[predict > self._thresholds].tolist()

    def _postprocess_predict_proba(
        self,
        predict: np.ndarray,
    ) -> Dict[str, float]:
        """Постобработка для получения словаря с вероятностями.

        Args:
            predict (np.ndarray): вероятности

        Returns:
            словарь вида класс: вероятность.
        """
        return {self._classes[i]: float(predict[i]) for i in predict.argsort()[::-1]}  # noqa: E501, WPS111
