from typing import Dict, List

import cv2
import numpy as np
import torch

from service.core.zoo import ModelZoo


class RainforestAnalytics:
    """Основной класс сервиса."""
    def __init__(self, config: Dict):
        """Метод инициализации и настройки сервиса.

        Args:
            config (Dict): словарь с настройками

        To use:
        config = {
            'max_threads': 1,
            'cold_run': True,
            'models': {
                'rainforest_classifier': {
                    'model_path': 'weights/classification_pt',
                    'device': "cuda:0",
                }  # noqa: RST203
            }
        }

        Привер использования:

        service = PosterAnalytics(config)
        classes = service.predict(image)
        """
        self.config = config
        self._setup_threads()
        self._zoo = ModelZoo(self.config['models'])
        if self.config.get('cold_run', True):
            self._cold_run()

    @property
    def classes(self):
        """Возвращает список классов.

        Returns:
            Список классов
        """
        return self._zoo.rainforest_classifier.classes

    def predict(self, image: np.ndarray) -> List[str]:
        """Предсказания лейблов.

        Args:
            image (np.ndarray): входное RGB изображение

        Returns:
            список классов
        """
        return self._zoo.rainforest_classifier.predict(image)

    def predict_proba(self, image: np.ndarray) -> Dict[str, float]:
        """Предсказание вероятностей принадлежности к классам.

        Args:
            image (np.ndarray): RGB изображение

        Returns:
            словарь вида класс: вероятность.
        """
        return self._zoo.rainforest_classifier.predict_proba(image)

    def _setup_threads(self):
        """Ограничиваем количество потоков для непитоновских библиотек."""
        max_threads = self.config['max_threads']
        cv2.setNumThreads(max_threads)
        torch.set_num_threads(max_threads)

    def _cold_run(self):
        """Инициализация всех моделей из зоопарка и прогон данных где это возможно."""  # noqa: E501
        models = [
            self._zoo.rainforest_classifier,
        ]

        for model in models:
            input_data = np.ones((*model.size, 3), dtype=np.uint8)
            model.predict(input_data)
