import cv2
import numpy as np
from dependency_injector.wiring import Provide, inject
from flask import request

from service.containers import Container
from service.core.core import RainforestAnalytics


@inject
def classes_list(service: RainforestAnalytics = Provide[Container.rainforest_analytics_service]):  # noqa: WPS404, E501, D103
    """Возвращает список классов.

    Args:
        service (RainforestAnalytics): контейнер с сервисом

    Returns:
        Список классов в словаре
    """
    return {'classes': service.classes}


@inject
def predict(service: RainforestAnalytics = Provide[Container.rainforest_analytics_service]):  # noqa: WPS404, E501, D103
    """Возвращает наиболее вероятные классы на картинке.

    Args:
        service (RainforestAnalytics): контейнер с сервисом

    Returns:
        Список наиболее вероятных классов для картинки в словаре
    """
    image = cv2.imdecode(
        np.frombuffer(request.data, np.uint8),
        cv2.IMREAD_COLOR,
    )
    labels = service.predict(image)
    return {'classes': labels}


@inject
def predict_proba(service: RainforestAnalytics = Provide[Container.rainforest_analytics_service]):  # noqa: WPS404, E501
    """Возвращает словарь с предиктами.

    Args:
        service (RainforestAnalytics): контейнер с сервисом

    Returns:
        словарь вида класс: вероятность.8
    """
    image = cv2.imdecode(
        np.frombuffer(request.data, np.uint8),
        cv2.IMREAD_COLOR,
    )
    return service.predict_proba(image)
