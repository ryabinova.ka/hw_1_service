def health_check():
    """Проверка того, что сервис работает.

    Returns:
        строка
    """
    return "I'm alive!"
