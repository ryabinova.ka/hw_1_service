from dependency_injector import containers, providers

from service.core.core import RainforestAnalytics


class Container(containers.DeclarativeContainer):
    """Контейнер для создания сервиса."""
    config = providers.Configuration()
    rainforest_analytics_service = providers.Factory(
        RainforestAnalytics,
        config=config.rainforest_analytics,
    )
